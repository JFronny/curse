﻿using System;
using System.Drawing;
using System.Reflection;
using System.Text;
using Microsoft.CodeAnalysis;

namespace SourceGenerators
{
    [Generator]
    class PensGenerator : ISourceGenerator
    {
        public void Initialize(GeneratorInitializationContext context)
        {
        }

        public void Execute(GeneratorExecutionContext context)
        {
            StringBuilder sb = new StringBuilder(@"using System.Drawing;
using Cosmos.System.Graphics;

namespace Curse
{
    public static class Pens
    {
");
            Type color = typeof(Color);
            foreach (PropertyInfo col in color.GetProperties())
            {
                if (col.PropertyType == color)
                    sb.Append($@"        public static readonly Pen {col.Name} = new Pen(Color.{col.Name});
");
            }

            sb.Append(@"    }
}");
            context.AddSource("Pens", sb.ToString());
        }
    }
}
