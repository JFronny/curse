﻿using System;
using System.Collections.Generic;
using Cosmos.System;
using Cosmos.System.Graphics.Fonts;
using Curse.Abstract;

namespace Curse.ImGUI
{
    public class WindowManager
    {
        private readonly List<Window> _windows;
        private readonly ImGuiProvider _provider;
        private readonly BufferCanvas _canvas;
        private readonly Font _font;
        private int _padding;
        public int Padding
        {
            get => _padding;
            set
            {
                _padding = value;
                TitlebarSize = _font.Height + value * 2;
                _provider.Padding = value;
            }
        }
        public int TitlebarSize { get; private set; }
        public bool MouseLocked
        {
            get
            {
                foreach (MouseLockProvider provider in LockProviders)
                {
                    if (provider.Locked)
                        return true;
                }
                return false;
            }
        }
        internal List<MouseLockProvider> LockProviders;
        private readonly MouseLockProvider _windowMovingLockProvider;
        readonly Random _rng = new Random();
        internal WindowManager(BufferCanvas canvas, Font font, int padding)
        {
            _canvas = canvas;
            _font = font;
            _provider = new ImGuiProvider(canvas, font, padding, this);
            Padding = padding;
            _windows = new List<Window>();
            LockProviders = new List<MouseLockProvider>();
            _windowMovingLockProvider = new MouseLockProvider(this);
        }

        public void Show(Window window)
        {
            window.WindowManager = this;
            window.Y += _font.Height * 2;
            bool contains;
            int id;
            do
            {
                id = _rng.Next() + 1;
                contains = false;
                foreach (Window wnd in _windows)
                {
                    if (wnd.Id == id)
                        contains = true;
                }
            } while (contains);
            window.Id = id;
            _windows.Add(window);
            window.Init();
        }

        public void Close(int id)
        {
            int c = _windows.Count;
            for (int i = c - 1; i >= 0; i--)
            {
                Window window = _windows[i];
                if (window.Id == id)
                {
                    window.Close();
                    _windows.RemoveAt(i);
                }
            }
        }

        public void Update()
        {
            foreach (Window window in _windows)
            {
                int headerY = window.Y - TitlebarSize;
                if (!MouseLocked && window.Movable && !window.IsMoving && MouseState.Left.Clicking() && Mouse.X >= window.X && Mouse.Y >= headerY && Mouse.X <= window.X + window.Width - _font.Height && Mouse.Y <= headerY + TitlebarSize)
                {
                    window.IsMoving = true;
                    _windowMovingLockProvider.Locked = true;
                    window.MovementOffsetX = window.X - Mouse.X;
                    window.MovementOffsetY = window.Y - Mouse.Y;
                }
                if (window.Movable)
                {
                    window.Y = Math.Min(Math.Max(window.Y, _font.Height + TitlebarSize), _canvas.ScreenY - window.Height);
                    window.X = Math.Min(Math.Max(window.X, 0), _canvas.ScreenX - window.Width);
                }

                if (window.Movable && window.IsMoving)
                {
                    window.X = Mouse.X + window.MovementOffsetX;
                    window.Y = Mouse.Y + window.MovementOffsetY;
                    _canvas.DrawRectangle(ColorConfs.Component, window.X, headerY, window.Width, window.Height + TitlebarSize);
                    if (!MouseState.Left.Down())
                    {
                        window.IsMoving = false;
                        _windowMovingLockProvider.Locked = false;
                    }
                    //TODO possibly move this out of here
                }
                else
                {
                    _provider.DrawForm(window);
                }
            }
        }

        public int GetWindow(int x, int y)
        {
            for (int i = _windows.Count - 1; i >= 0; i--)
            {
                Window window = _windows[i];
                if (window.Movable)
                {
                    if (Contains(x, y, window.X, window.Y - TitlebarSize, window.Width, window.Height + TitlebarSize))
                        return window.Id;
                }
                else
                {
                    if (Contains(x, y, window.X, window.Y, window.Width, window.Height))
                        return window.Id;
                }
            }
            return -1;
        }

        public bool ContainsMouse(int x, int y, int w, int h)
        {
            return Contains(Mouse.X, Mouse.Y, x, y, w, h);
        }

        public bool Contains(int px, int py, int rx, int ry, int rw, int rh)
        {
            return rx <= px
                && ry <= py
                && rx + rw >= px
                && ry + rh >= py;
        }

        public void ForceUpdate()
        {
            _canvas.ClearImmediate();
        }
    }
}
