﻿using System;

namespace Curse.ImGUI
{
    public class MouseLockProvider : IDisposable
    {
        private readonly WindowManager _manager;
        public bool Locked = false;

        public MouseLockProvider(WindowManager manager)
        {
            _manager = manager;
            manager.LockProviders.Add(this);
        }

        public void Dispose()
        {
            for (int i = _manager.LockProviders.Count - 1; i >= 0; i--)
            {
                if (_manager.LockProviders[i] == this)
                    _manager.LockProviders.RemoveAt(i);
            }
        }
    }
}
