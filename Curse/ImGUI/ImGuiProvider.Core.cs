﻿using System;
using Cosmos.System.Graphics;
using Cosmos.System.Graphics.Fonts;
using Curse.Abstract;

namespace Curse.ImGUI
{
    public partial class ImGuiProvider
    {
        public Canvas Canvas { get; }
        public readonly Font Font;
        public int Padding { get; set; }
        private readonly WindowManager _manager;
        internal ImGuiProvider(Canvas canvas, Font font, int padding, WindowManager manager)
        {
            Canvas = canvas;
            Font = font;
            Padding = padding;
            _manager = manager;
        }

        internal void DrawForm(Window window)
        {
            _heightCurrent = 0;
            _widthCurrent = TextSize(window.GetTitle()) + Padding + Font.Height * 2;
            _columnStart = Padding;
            _columnWidth = Padding;
            _columnStartY = Padding;
            _maxColumnHeight = 0;
            _rowHeight = Padding;
            _isRow = false;
            _startX = Padding;
            _startY = Padding;
            _xOff = window.X;
            _yOff = window.Y;
            _ignoreLock = false;
            _mouseOverCurrent = _manager.GetWindow(Mouse.X, Mouse.Y) == window.Id;
            try
            {
                window.Update(this);
                _startX = 0;
                _startY = -_manager.TitlebarSize;
                window.DrawDecorations(this, _manager.TitlebarSize);
            }
            catch (Exception e)
            {
                Label(e.ToString().PrintDebug());
            }
            _widthCurrent += Padding;
            _heightCurrent += Padding;
            window.Height = _heightCurrent;
            window.Width = _widthCurrent;
        }

        private bool _mouseOverCurrent;

        public void DrawRectangle(int x, int y, int w, int h, Pen pen, bool filled) => DrawRectangleAbs(x + _startX, y + _startY, w, h, pen, filled);

        public void DrawRectangleAbs(int x, int y, int w, int h, Pen pen, bool filled)
        {
            x += _xOff;
            y += _yOff;
            if (filled)
                Canvas.DrawFilledRectangle(pen, x, y, w, h);
            else
                Canvas.DrawRectangle(pen, x, y, w, h);
        }

        public void DrawText(int x, int y, Pen pen, string text)
        {
            Canvas.DrawString(text, Font, pen, _xOff + _startX + x, _yOff + _startY + y);
        }
    }
}
