﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Cosmos.System;
using Cosmos.System.Graphics;
using Curse.Abstract;

//TODO rename *Color to *Pen
namespace Curse.ImGUI
{
    public partial class ImGuiProvider
    {
        public bool Button(string text, Pen foreColor = null, Pen backColor = null, Pen backColorHover = null,
            bool repeat = false, bool hoverActivate = false)
        {
            foreColor ??= ColorConfs.Text;
            backColor ??= ColorConfs.Component;
            backColorHover ??= ColorConfs.Accent;
            int w = TextSize(text);
            int h = Font.Height + Padding * 2;
            BeginComponent(w, h);
            bool hover = _mouseOverCurrent && ContainsMouse(0, 0, w, h);
            bool ret = false;
            if (hover)
            {
                if (MouseState.Left.Down() || hoverActivate)
                {
                    DrawRectangle(0, 0, w, h, backColorHover, true);
                    if (repeat || hoverActivate || MouseState.Left.Clicking())
                        ret = true;
                }
                else
                {
                    DrawRectangle(0, 0, w, h, backColor, true);
                }
            }
            else
            {
                DrawRectangle(0, 0, w, h, backColor, false);
            }

            DrawText(Padding, Padding, foreColor, text);
            FinishComponent(w, h);
            return ret;
        }

        public bool Checkbox(bool value, string text, Pen textColor = null, Pen backColor = null,
            Pen backColorHover = null)
        {
            textColor ??= ColorConfs.Text;
            backColor ??= ColorConfs.Component;
            backColorHover ??= ColorConfs.Accent;
            int w = Font.Height + Padding * 2 + TextSize(text);
            int h = Font.Height + Padding * 2;
            BeginComponent(w, h);
            bool hover = _mouseOverCurrent && ContainsMouse(0, 0, w, h);
            bool ret = false;
            if (hover)
            {
                if (MouseState.Left.Down())
                {
                    DrawRectangle(0, 0, h, h, backColorHover, true);
                    if (MouseState.Left.Clicking())
                        ret = true;
                }
                else
                {
                    DrawRectangle(0, 0, h, h, backColor, true);
                }
            }
            else
            {
                DrawRectangle(0, 0, h, h, backColor, false);
            }

            if (value)
                DrawText(Padding + h / 2 - Font.Width / 2, Padding + h / 2 - Font.Height / 2, textColor, "X");
            DrawText(Font.Height + Padding * 2, Padding, textColor, text);
            FinishComponent(w, h);
            return ret ? !value : value;
        }

        public void Label(string text, Pen color = null)
        {
            color ??= ColorConfs.Text;
            string[] lines = text.Split('\n');
            int w = MaxLen(lines);
            int h = Font.Height * lines.Length;
            BeginComponent(w, h);
            for (var i = 0; i < lines.Length; i++)
            {
                DrawText(Padding, Padding + Font.Height * i, color, text);
            }
            FinishComponent(w, h);
        }

        private int MaxLen(string[] strings)
        {
            int q = 0;
            foreach (string s in strings)
            {
                int w = TextSize(s);
                if (w > q)
                    q = w;
            }

            return q;
        }

        public int Scrollbar(int value, int min, int max, int w = 100, Pen colorActive = null,
            Pen colorInactive = null, Pen textColor = null, bool showText = true)
        {
            colorActive ??= ColorConfs.Accent;
            colorInactive ??= ColorConfs.Component;
            textColor ??= ColorConfs.Text;
            int wPart = (int) Math.Round((max * 1f - value) / (max - min) * (w - Padding * 2));
            int h = Font.Height + Padding * 2;
            BeginComponent(w, h);
            DrawRectangle(0, 0, w, h, colorActive, true);
            DrawRectangle(w - wPart - Padding, Padding, wPart, Font.Height, colorInactive, true);
            if (showText)
                DrawText(Padding, Padding, textColor, value.ToString());
            int ret;
            if (_mouseOverCurrent && ContainsMouse(Padding, Padding, w - Padding * 2, Font.Height))
            {
                if (MouseState.Left.Down())
                {
                    ret = (int) Math.Round(min + (max - min) * ((MouseXRelative - Padding) * 1f / (w - Padding)));
                }
                else
                {
                    ret = value;
                }
            }
            else
            {
                ret = value;
            }

            FinishComponent(w, h);
            return ret;
        }

        public float Scrollbar(float value, float min, float max, int w = 100, Pen colorActive = null,
            Pen colorInactive = null, Pen textColor = null, bool showText = true)
        {
            colorActive ??= ColorConfs.Accent;
            colorInactive ??= ColorConfs.Component;
            textColor ??= ColorConfs.Text;
            int wPart = (int) Math.Round((max - value) / (max - min) * (w - Padding * 2));
            int h = Font.Height + Padding * 2;
            BeginComponent(w, h);
            DrawRectangle(0, 0, w, h, colorActive, true);
            DrawRectangle(w - wPart - Padding, Padding, wPart, Font.Height, colorInactive, true);
            if (showText)
                DrawText(Padding, Padding, textColor, value.ToString());
            float ret;
            if (_mouseOverCurrent && ContainsMouse(Padding, Padding, w - Padding * 2, Font.Height))
            {
                if (MouseState.Left.Down())
                {
                    ret = min + (max - min) * ((MouseXRelative - Padding) * 1f / (w - Padding));
                }
                else
                {
                    ret = value;
                }
            }
            else
            {
                ret = value;
            }

            FinishComponent(w, h);
            return ret;
        }

        public Color ColorSelector(Color current)
        {
            int sW = 100;
            int w = Padding * 2 + Font.Height + sW;
            int h = Font.Height * 3 + Padding * 6;
            BeginComponent(w, h);
            DrawRectangle(0, 0, Font.Height, h, new Pen(current), true);
            DrawRectangle(0, 0, Font.Height, h, Pens.Black, false);
            _startX += Padding + Font.Height;
            int r = Scrollbar(current.R, 0, byte.MaxValue, sW, Pens.Red, Pens.DarkRed, Pens.White);
            _startX += Padding + Font.Height;
            int g = Scrollbar(current.G, 0, byte.MaxValue, sW, Pens.Green, Pens.DarkGreen, Pens.White);
            _startX += Padding + Font.Height;
            int b = Scrollbar(current.B, 0, byte.MaxValue, sW, Pens.Blue, Pens.DarkBlue, Pens.White);
            return Color.FromArgb(r, g, b);
        }

        public string TextBoxSingle(string text, ref int cursor, Pen backColor = null, Pen highlightColor = null, Pen textColor = null, char[] bannedChars = null)
        {
            backColor ??= ColorConfs.Component;
            highlightColor ??= ColorConfs.Accent;
            textColor ??= ColorConfs.Text;
            bannedChars ??= new char[0];
            int w = Math.Max(TextSize(text), Font.Width * 4);
            int h = Font.Height + Padding * 2;
            BeginComponent(w, h);
            bool mouse = ContainsMouse(0, 0, w, h);
            Pen baseColor = (mouse ? highlightColor : backColor);
            DrawRectangle(0, 0, w, h, baseColor, false);
            DrawText(Padding, Padding, textColor, text);
            if (mouse)
            {
                if (MouseState.Left.Down())
                    cursor = (int)Math.Round((MouseXRelative - Padding) * 1f / Font.Width);
                if (cursor > text.Length) cursor = text.Length;
                if (cursor < 0) cursor = 0;
                DrawRectangle(cursor * Font.Width + Padding, 0, 2, Font.Height + Padding * 2, baseColor, true);
                foreach (var key in Keyboard.Input)
                {
                    switch (key.Key)
                    {
                        case ConsoleKey.LeftArrow:
                        case ConsoleKey.UpArrow:
                            cursor--;
                            break;
                        case ConsoleKey.RightArrow:
                        case ConsoleKey.DownArrow:
                            cursor++;
                            break;
                        case ConsoleKey.Delete:
                            if (cursor < text.Length)
                            {
                                text = text.Substring(0, cursor) + text.Substring(cursor + 1);
                            }

                            break;
                        case ConsoleKey.Backspace:
                            if (cursor > 0)
                            {
                                text = text.Substring(0, cursor - 1) + text.Substring(cursor);
                                cursor--;
                            }

                            break;
                        default:
                            if (key.KeyChar.IsText())
                            {
                                bool add = true;
                                foreach (var c in bannedChars)
                                {
                                    if (c == key.KeyChar)
                                        add = false;
                                }
                                if (add)
                                {
                                    text = text.Substring(0, cursor) + key.KeyChar + text.Substring(cursor);
                                    cursor++;
                                }
                            }

                            break;
                    }
                    if (cursor > text.Length) cursor = text.Length;
                    if (cursor < 0) cursor = 0;
                }
            }
            FinishComponent(w, h);
            return text;
        }

        public void TextBoxMulti(List<string> text, ref int cursorX, ref int cursorY, Pen backColor = null, Pen highlightColor = null, Pen textColor = null)
        {
            backColor ??= ColorConfs.Component;
            highlightColor ??= ColorConfs.Accent;
            textColor ??= ColorConfs.Text;
            if (text.Count == 0)
                text.Add("");
            int w = Font.Width * 4;
            int h = (int)Math.Round(Font.Height * Math.Max(text.Count, 1.5f)) + Padding * 2;
            BeginComponent(w, h);
            for (var i = text.Count - 1; i >= 0; i--)
            {
                string s = text[i];
                if (TextSize(s) > w)
                    w = TextSize(s);
                DrawText(Padding, Padding + i * Font.Height, textColor, s);
            }
            bool mouse = ContainsMouse(0, 0, w, h);
            Pen baseColor = (mouse ? highlightColor : backColor);
            DrawRectangle(0, 0, w, h, baseColor, false);
            if (mouse)
            {
                if (MouseState.Left.Down())
                {
                    cursorX = (int)Math.Round((MouseXRelative - Padding) * 1f / Font.Width);
                    cursorY = (int) Math.Round(MouseYRelative * 1f / Font.Height);
                }
                if (cursorY >= text.Count) cursorY = text.Count - 1;
                if (cursorY < 0) cursorY = 0;
                if (cursorX > text[cursorY].Length) cursorY = text[cursorY].Length;
                if (cursorX < 0) cursorX = 0;
                DrawRectangle(cursorX * Font.Width + Padding, cursorY * Font.Height, 2, Font.Height + Padding * 2, baseColor, true);
                foreach (var key in Keyboard.Input)
                {
                    switch (key.Key)
                    {
                        case ConsoleKey.LeftArrow:
                            cursorX--;
                            if (cursorX < 0 && cursorY > 0)
                            {
                                cursorY--;
                                cursorX = text[cursorY].Length;
                            }
                            break;
                        case ConsoleKey.UpArrow:
                            cursorY--;
                            if (cursorY < 0)
                                cursorX = 0;
                            break;
                        case ConsoleKey.RightArrow:
                            cursorX++;
                            if (cursorX > text[cursorY].Length && cursorY < text.Count - 1)
                            {
                                cursorY++;
                                cursorX = 0;
                            }
                            break;
                        case ConsoleKey.DownArrow:
                            cursorY++;
                            if (cursorY >= text.Count)
                                cursorX = text[text.Count - 1].Length;
                            break;
                        case ConsoleKey.Home:
                            cursorX = 0;
                            break;
                        case ConsoleKey.End:
                            cursorX = text[cursorY].Length;
                            break;
                        case ConsoleKey.Delete:
                            if (cursorX < text[cursorY].Length)
                            {
                                text[cursorY] = text[cursorY].Substring(0, cursorX) +
                                                text[cursorY].Substring(cursorX + 1);
                            }
                            else if (cursorY < text.Count - 1)
                            {
                                text[cursorY] += text[cursorY + 1];
                                text.RemoveAt(cursorY + 1);
                            }
                            break;
                        case ConsoleKey.Backspace:
                            if (cursorX > 0)
                            {
                                text[cursorY] = text[cursorY].Substring(0, cursorX - 1) +
                                                text[cursorY].Substring(cursorX);
                                cursorX--;
                            }
                            else if (cursorY > 0)
                            {
                                cursorX = text[cursorY - 1].Length;
                                text[cursorY - 1] += text[cursorY];
                                text.RemoveAt(cursorY);
                                cursorY--;
                            }
                            break;
                        case ConsoleKey.Enter:
                            text.Insert(cursorY + 1, text[cursorY].Substring(cursorX));
                            text[cursorY] = text[cursorY].Substring(0, cursorX);
                            cursorX = 0;
                            cursorY++;
                            break;
                        default:
                            if (key.KeyChar.IsText())
                            {
                                text[cursorY] = text[cursorY].Substring(0, cursorX) + key.KeyChar +
                                                text[cursorY].Substring(cursorX);
                                cursorX++;
                            }
                            break;
                    }
                    if (cursorY >= text.Count) cursorY = text.Count - 1;
                    if (cursorY < 0) cursorY = 0;
                    if (cursorX > text[cursorY].Length) cursorX = text[cursorY].Length;
                    if (cursorX < 0) cursorX = 0;
                }
            }
            FinishComponent(w, h);
        }
    }
}
