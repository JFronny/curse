﻿using System;
using Curse.Abstract;

namespace Curse.ImGUI
{
    public partial class ImGuiProvider
    {
        private int _widthCurrent; // The current width of the window, updated when adding components
        private int _heightCurrent; // The current height of the window, updated when adding components
        private int _startX; // The X position the current component starts at, relative to xOff
        private int _startY; // The Y position the current component starts at, relative to yOff
        private int _xOff; // The absolute X position of the current window relative to the workspace origin
        private int _yOff; // The absolute Y position of the current window relative to the workspace origin
        private int MouseXRelative => Mouse.X - _xOff - _startX; // The mouses X position relative to the current component
        private int MouseYRelative => Mouse.Y - _yOff - _startY; // The mouses Y position relative to the current component
        private int _columnStart; // Horizontal start offset for the current column
        private int  _columnWidth; // Contains the maximum width for an element in the current column, used when creating a new one
        private int _columnStartY; // The vertical offset where the current set of columns was started, reset in ResetColumn and used to set StartY in Column()
        private int _maxColumnHeight; // Contains the maximum height for a column. Only if this is surpassed in the current column (checked via StartY - ColumnStartY) is HeightCurrent increased
        private int ColumnHeight => _startY - _columnStartY;
        private bool _isRow; // Set to true if the next element should be right of the current one instead of below
        private int _rowHeight; // Height of the current row. Added to StartY after the row is ended
        private bool _ignoreLock; // Used to indicate that mouse locks will be ignored by this GUI
        public void IgnoreLock(bool value = true) => _ignoreLock = value;

        public bool ContainsMouse(int x, int y, int w, int h, bool offset = true)
        {
            if (offset)
            {
                x += _startX + _xOff;
                y += _startY + _yOff;
            }

            return (_ignoreLock || !_manager.MouseLocked) && _manager.ContainsMouse(x, y, w, h);
        }

        private int TextSize(string text)
        {
            return text.Length * Font.Width + Padding * 2;
        }

        private void BeginComponent(int w, int h)
        {
            int wNew = w + _startX;
            if (_widthCurrent < wNew)
            {
                DrawRectangleAbs(_widthCurrent + 1, 0, wNew - _widthCurrent, _heightCurrent + 1, ColorConfs.Background, true);
                _widthCurrent = wNew;
            }

            int hNew = h + _startY;
            if (_heightCurrent < hNew)
            {
                DrawRectangleAbs(1, _heightCurrent + 1, _widthCurrent, hNew - _heightCurrent, ColorConfs.Background, true);
                _heightCurrent = hNew;
            }

            wNew -= _columnStart;
            if (_columnWidth < wNew)
            {
                DrawRectangleAbs(_columnStart + _columnWidth + 1, _columnStartY, wNew - _columnWidth, Math.Max(_maxColumnHeight, ColumnHeight), ColorConfs.Background, true);
                _columnWidth = wNew;
            }

            if (_isRow && _rowHeight < h + Padding)
            {
                DrawRectangle(-_startX, _rowHeight, _columnWidth, _rowHeight - h - Padding, ColorConfs.Background, true);
                _rowHeight = h + Padding;
            }
            DrawRectangle(-Padding + 1, -Padding + 1, _widthCurrent - _startX + Padding * 2, (_isRow ? _rowHeight : h) + Padding, ColorConfs.Background, true);
        }

        private void FinishComponent(int w, int h)
        {
            if (_isRow)
            {
                _startX += w + Padding;
                if (_rowHeight < h + Padding)
                    _rowHeight = h + Padding;
            }
            else
            {
                _startX = _columnStart;
                _startY += h + Padding;
            }
        }

        public void StartRow() => _isRow = true;

        public void EndRow()
        {
            if (!_isRow) return;
            _isRow = false;
            _startX = _columnStart;
            _startY += _rowHeight + Padding;
            _rowHeight = Padding;
        }

        public void Column()
        {
            EndRow();
            _columnStart += _columnWidth + Padding;
            _columnWidth = Padding;
            if (ColumnHeight > _maxColumnHeight) _maxColumnHeight = ColumnHeight;
            _startY = _columnStartY;
            _startX = _columnStart;
        }

        public void ResetColumn()
        {
            Column();
            _columnStart = Padding;
            _columnWidth = Padding;
            _startY = _maxColumnHeight;
            _startX = Padding;
            _columnStartY = _startY;
            _maxColumnHeight = 0;
        }
    }
}