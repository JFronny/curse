﻿using System;
using System.Drawing;
using Cosmos.System.FileSystem;
using Cosmos.System.FileSystem.Listing;
using Cosmos.System.FileSystem.VFS;
using Cosmos.System.Graphics;
using Cosmos.System.Graphics.Fonts;
using Curse.Abstract;
using Curse.ImGUI;
using Curse.VFS;
using Curse.Windows;
using Keyboard = Curse.Abstract.Keyboard;
using Mouse = Curse.Abstract.Mouse;
using Sys = Cosmos.System;

namespace Curse
{
    public class Kernel : Sys.Kernel
    {
        private Canvas _canvas;
        private BufferCanvas _bufferCanvas;
        private PCScreenFont _font;
        private WindowManager _manager;
        public MountFileSystem FileSystem;
        public static Kernel Instance;

        protected override void BeforeRun()
        {
            Instance = this;
            CosmosVFS fs = new CosmosVFS();
            VFSManager.RegisterVFS(fs);
            "Cosmos VFS initialized, creating MountFS".PrintDebug();
            FileSystem = new MountFileSystem(FsPath.Root);
            "Creating /mnt".PrintDebug();
            MountFileSystem mnt = new MountFileSystem(FsPath.Mnt);
            "Creating /res".PrintDebug();
            ResFileSystem res = new ResFileSystem(Resources.Files, FsPath.Res);
            "Registering mounts for /".PrintDebug();
            FileSystem.Mounts.Add(FsPath.Mnt.Path, mnt);
            "[a]".PrintDebug();
            FileSystem.Mounts.Add(FsPath.Res.Path, res);
            "Registering mounts for /mnt".PrintDebug();
            foreach (DirectoryEntry entry in VFSManager.GetVolumes())
            {
                $"Registering {entry.mFullPath}".PrintDebug();
                FsPath fp = FsPath.Mnt.AppendDirectory(entry.mName);
                mnt.Mounts.Add(fp.Path, new CosmosMountFileSystem(entry, fp));
            }
            "Initializing graphics".PrintDebug();
            _canvas = FullScreenCanvas.GetFullScreenCanvas();
            _font = PCScreenFont.Default;
            _bufferCanvas = new BufferCanvas(_canvas);
            "Showing mode selector".PrintDebug();
            SetGraphicsMode(_bufferCanvas.ShowSelectPrompt(_font));
            "Initializing shell".PrintDebug();
            _manager = new WindowManager(_bufferCanvas, _font, 2);
            _manager.Show(new Desktop());
        }

        protected override void Run()
        {
            try
            {
                _bufferCanvas.Clear(ColorConfs.Background.Color);
                Mouse.UpdateState();
                Keyboard.UpdateState();
                Backdrop.Draw(_bufferCanvas, _font, _manager);
                _manager.Update();
                Mouse.Draw(_bufferCanvas);
                _bufferCanvas.Draw();
            }
            catch (Exception e)
            {
                _canvas.Clear(Color.Red);
                Pen error = Pens.White;
                string[] ses = e.ToString().Split('\n');
                for (int i = 0; i < ses.Length; i++)
                {
                    string s = ses[i];
                    _canvas.DrawString(s, _font, error, 0, i * _font.Height);
                }
            }
        }

        public void Iter() => Run();

        public void SetGraphicsMode(Mode mode)
        {
            _canvas.Mode = mode;
            _bufferCanvas.ClearImmediate();
            Sys.MouseManager.ScreenWidth = Convert.ToUInt32(_bufferCanvas.ScreenX);
            Sys.MouseManager.ScreenHeight = Convert.ToUInt32(_bufferCanvas.ScreenY);
        }
    }
}
