﻿using System;
using System.Collections.Generic;

namespace Curse.Abstract
{
    public static class Keyboard
    {
        private static readonly List<ConsoleKeyInfo> InputBuffer = new List<ConsoleKeyInfo>();
        internal static void UpdateState()
        {
            InputBuffer.Clear();
            while (Console.KeyAvailable)
            {
                InputBuffer.Add(Console.ReadKey());
            }
        }

        /*public static string GetString(bool multiline)
        {
            string text = "";
            foreach (var t in GetChars())
            {
                if (multiline || t != '\n')
                    text += t;
            }
            return text;
        }

        public static char[] GetChars()
        {
            List<char> chars = new List<char>();
            foreach (var t in _inputBuffer)
            {
                if (char.IsLetterOrDigit(t.KeyChar))
                    chars.Add(t.KeyChar);
            }
            return chars.ToArray();
        }*/

        public static ConsoleKeyInfo[] Input => InputBuffer.ToArray();
    }
}
