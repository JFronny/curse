﻿using System;
using Cosmos.System;
using Cosmos.System.Graphics;

namespace Curse.Abstract
{
    public static class Mouse
    {
        internal static void UpdateState()
        {
            _statePrevious = _stateCurrent;
            _stateCurrent = MouseManager.MouseState;
            X = Convert.ToInt32(MouseManager.X);
            Y = Convert.ToInt32(MouseManager.Y);
        }

        public static Style MouseStyle = Style.Simple;
        public static void Draw(Canvas canvas)
        {
            Pen pen = ColorConfs.Text;
            switch (MouseStyle)
            {
                case Style.Classic:
                    canvas.DrawLine(pen, X, Y, X + 10, Y + 15);
                    canvas.DrawTriangle(pen, X, Y, X + 5, Y, X, Y + 10);
                    break;
                case Style.Vertical:
                    canvas.DrawTriangle(pen, X, Y, X + 5, Y + 5, X - 5, Y + 5);
                    canvas.DrawLine(pen, X, Y + 5, X, Y + 15);
                    break;
                case Style.Simple:
                    canvas.DrawTriangle(pen, X, Y, X + 10, Y, X, Y + 10);
                    canvas.DrawLine(pen, X + 5, Y + 5, X + 15, Y + 15);
                    break;
                case Style.Crosshair:
                    canvas.DrawLine(pen, X - 10, Y, X + 10, Y);
                    canvas.DrawLine(pen, X, Y - 10, X, Y + 10);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static MouseState _statePrevious = MouseState.None;
        private static MouseState _stateCurrent = MouseState.None;
        public static int X { get; private set; }
        public static int Y { get; private set; }

        public static bool Down(this MouseState state)
        {
            return _stateCurrent.Contains(state);
        }
        public static bool Clicking(this MouseState state)
        {
            return _stateCurrent.Contains(state) && !_statePrevious.Contains(state);
        }

        private static bool Contains(this MouseState s1, MouseState s2) => (s1 & s2) == s2;

        public enum Style
        {
            Classic,
            Vertical,
            Simple,
            Crosshair
        }
    }
}
