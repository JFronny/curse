﻿using System;
using System.Collections.Generic;
using System.IO;
using Curse.ImGUI;
using Curse.VFS;
using Curse.ZMachine;

namespace Curse.Windows
{
    public class Zork : Window
    {
        private ZMachineMain _machine;
        private bool _requestingInput = false;
        private Action<string, byte> _callback;
        private string _input = "";
        private int _cursor = 0;
        private readonly List<string> _console2 = new List<string> {""};
        private readonly IFileSystem _fs = Kernel.Instance.FileSystem;
        public override void Init()
        {
        }

        public override void Close()
        {
        }

        public override void Update(ImGuiProvider imGui)
        {
            if (_machine == null)
            {
                foreach (var entry in _fs.GetEntries(FsPath.Res))
                {
                    if (entry.IsFile && entry.EntityName.EndsWith(".z5") && imGui.Button(entry.EntityName.Substring(0, entry.EntityName.Length - 3)))
                    {
                        byte[] content;
                        using (Stream s = _fs.OpenFile(entry))
                        {
                            content = new byte[s.Length];
                            s.Read(content, 0, (int)s.Length);
                        }

                        _machine = new ZMachineMain(content, this);
                        _machine.Run();
                    }
                }
            }
            else
            {
                int end = _console2.Count - 1;
                for (var i = end; i >= 0; i--)
                {
                    if (end - i > 12)
                        _console2.RemoveAt(i);
                }
                foreach (var s in _console2)
                {
                    imGui.Label(s);
                }

                if (_requestingInput)
                    _input = imGui.TextBoxSingle(_input, ref _cursor);
                if (imGui.Button("OK"))
                {
                    WriteString(_input + '\n');
                    _cursor = 0;
                    byte aTerminator = 13;
                    _callback(_input, aTerminator);
                }
            }
        }

        public override string GetTitle() => nameof(Zork);
        
        public void ReadLine(string aInitial, int aTimeout, ushort aTimeoutCallback, byte[] aTerminatingKeys, Action<string, byte> callback)
        {
            _callback = callback;
            _requestingInput = true;
            _input = aInitial;
        }

        public void WriteChar(char aC)
        {
            if (aC == '\n')
            {
                if (!string.IsNullOrWhiteSpace(_console2[_console2.Count - 1]))
                    _console2.Add("");
            }
            else
                _console2[_console2.Count - 1] += aC;
        }

        public void WriteString(string aS)
        {
            if (_console2[_console2.Count - 1] != aS)
                foreach (var t in aS)
                    WriteChar(t);
            Kernel.Instance.Iter();
        }
    }
}
