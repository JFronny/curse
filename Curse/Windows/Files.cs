﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Cosmos.System.FileSystem.VFS;
using Curse.ImGUI;
using Curse.VFS;

namespace Curse.Windows
{
    class Files : Window
    {
        public FsPath Path = FsPath.Root;
        private List<string> _content;
        private int _cursorX = 0;
        private int _cursorY = 0;
        private MessageType _messageType;
        private string _message = null;
        private string _inputText = "";
        private int _inputCursor = 0;
        private char[] _invalidChars;
        private string[] _allowedExtensions = { ".txt" };
        private IFileSystem fs;
        public override void Init()
        {
            Path = FsPath.Root;
            _invalidChars = VFSManager.GetInvalidFileNameChars();
            fs = Kernel.Instance.FileSystem;
        }

        public override void Close()
        {
        }

        public override void Update(ImGuiProvider imGui)
        {
            "[str]".PrintDebug();
            try
            {
                imGui.StartRow();
                if (Path != FsPath.Root && imGui.Button(".."))
                {
                    Path = Path.ParentPath;
                    _content = null;
                    _cursorX = 0;
                    _cursorY = 0;
                }

                if (_message != null)
                {
                    imGui.EndRow();
                    imGui.Label(_message);
                    switch (_messageType)
                    {
                        case MessageType.MsgBox:
                            if (imGui.Button("OK"))
                                _message = null;
                            break;
                        case MessageType.CreateFileBox:
                            _inputText = imGui.TextBoxSingle(_inputText, ref _inputCursor, bannedChars: _invalidChars);
                            if (imGui.Button("OK"))
                            {
                                bool hasValidExtension = false;
                                foreach (var extension in _allowedExtensions)
                                {
                                    if (_inputText.EndsWith(extension))
                                        hasValidExtension = true;
                                }

                                if (!hasValidExtension)
                                    _inputText += ".txt";
                                
                                Path = Path.AppendFile(_inputText);
                                fs.CreateEmpty(Path);
                                _message = null;
                            }

                            break;
                        case MessageType.CreateDirBox:
                            _inputText = imGui.TextBoxSingle(_inputText, ref _inputCursor, bannedChars: _invalidChars);
                            if (imGui.Button("OK"))
                            {
                                Path = Path.AppendDirectory(_inputText);
                                fs.CreateEmpty(Path);
                                _message = null;
                            }
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
                else
                {
                    if (!fs.ReadOnly(Path))
                    {
                        if (imGui.Button("Delete"))
                        {
                            fs.Delete(Path);
                            Path = Path.ParentPath;
                            _content = null;
                            _cursorX = 0;
                            _cursorY = 0;
                            return;
                        }
                    }
                    if (Path.IsDirectory)
                    {
                        if (!fs.ReadOnly(Path))
                        {
                            imGui.Label("Create");
                            if (imGui.Button("Directory"))
                            {
                                _messageType = MessageType.CreateDirBox;
                                _message = "Please enter your new directories name";
                                _inputText = "";
                                _inputCursor = 0;
                            }
                            if (imGui.Button("File"))
                            {
                                _messageType = MessageType.CreateFileBox;
                                _message = "Please enter your new files name";
                                _inputText = "";
                                _inputCursor = 0;
                            }
                        }
                        imGui.EndRow();
                        foreach (FsPath path in fs.GetEntries(Path))
                        {
                            if (imGui.Button(path.EntityName + (path.IsDirectory ? "/" : "")))
                                Path = path;
                        }
                    }
                    else
                    {
                        bool validExtension = false;
                        foreach (var extension in _allowedExtensions)
                        {
                            if (Path.EntityName.EndsWith(extension))
                                validExtension = true;
                        }

                        if (!validExtension)
                        {
                            imGui.EndRow();
                            imGui.Label(
                                "The selected file has an unknown extension and will not be opened to prevent damage");
                        }
                        else
                        {
                            if (imGui.Button("Save"))
                            {
                                string f = "";
                                for (var i = 0; i < _content.Count; i++)
                                {
                                    f += _content[i];
                                    if (i < _content.Count - 1)
                                        f += '\n';
                                }

                                using Stream s = fs.OpenFile(Path);
                                byte[] data = Encoding.ASCII.GetBytes(f);
                                s.Write(data, 0, data.Length);
                            }

                            imGui.EndRow();
                            if (_content == null)
                            {
                                byte[] data;
                                using (Stream stream = fs.OpenFile(Path))
                                {
                                    data = new byte[stream.Length];
                                    stream.Read(data, 0, (int) stream.Length);
                                }

                                string current = "";
                                _content = new List<string>();
                                foreach (char c in data)
                                {
                                    if (c == '\n')
                                    {
                                        _content.Add(current);
                                        current.PrintDebug();
                                        current = "";
                                    }
                                    else
                                    {
                                        current += c;
                                    }
                                }

                                current.PrintDebug();
                                _content.Add(current);
                            }

                            imGui.TextBoxMulti(_content, ref _cursorX, ref _cursorY);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _message = e.ToString();
                _messageType = MessageType.MsgBox;
            }
        }

        public override string GetTitle() => Path == null || Path.Path == null ? nameof(Files) :  $"{nameof(Files)} - {Path.Path}";

        enum MessageType
        {
            MsgBox,
            CreateFileBox,
            CreateDirBox
        }
    }
}
