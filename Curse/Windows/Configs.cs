﻿using System.Drawing;
using Cosmos.System;
using Cosmos.System.Graphics;
using Cosmos.System.ScanMaps;
using Curse.Abstract;
using Curse.ImGUI;

namespace Curse.Windows
{
    class Configs : Window
    {
        private Color _backdropColor;
        private Color _textColor;
        private Color _backgroundColor;
        private Color _hoverColor;
        private int _padding;
        private float _sensitivity;
        public override void Close()
        {
        }

        public override string GetTitle()
        {
            return nameof(Configs);
        }

        public override void Init()
        {
            _backdropColor = ColorConfs.Background.Color;
            _textColor = ColorConfs.Text.Color;
            _backgroundColor = ColorConfs.Component.Color;
            _hoverColor = ColorConfs.Accent.Color;
            _padding = WindowManager.Padding;
            _sensitivity = MouseManager.MouseSensitivity;
        }

        public override void Update(ImGuiProvider imGui)
        {
            imGui.Label("GUI");
            imGui.Label(nameof(ColorConfs.Background));
            _backdropColor = imGui.ColorSelector(_backdropColor);
            imGui.Label(nameof(ColorConfs.Text));
            _textColor = imGui.ColorSelector(_textColor);
            imGui.Label(nameof(ColorConfs.Component));
            _backgroundColor = imGui.ColorSelector(_backgroundColor);
            imGui.Label(nameof(ColorConfs.Accent));
            _hoverColor = imGui.ColorSelector(_hoverColor);
            imGui.Label("Padding");
            _padding = imGui.Scrollbar(_padding, 0, 10);
            imGui.Label("Sensitivity");
            _sensitivity = imGui.Scrollbar(_sensitivity, 0.5f, 2f);
            imGui.Label("Mouse Style");
            if (imGui.Button(nameof(Mouse.Style.Simple)))
                Mouse.MouseStyle = Mouse.Style.Simple;
            if (imGui.Button(nameof(Mouse.Style.Classic)))
                Mouse.MouseStyle = Mouse.Style.Classic;
            if (imGui.Button(nameof(Mouse.Style.Crosshair)))
                Mouse.MouseStyle = Mouse.Style.Crosshair;
            if (imGui.Button(nameof(Mouse.Style.Vertical)))
                Mouse.MouseStyle = Mouse.Style.Vertical;
            imGui.Label("Language");
            imGui.StartRow();
            if (imGui.Button("US")) Global.ChangeKeyLayout(new US_Standard());
            if (imGui.Button("DE")) Global.ChangeKeyLayout(new DE_Standard());
            if (imGui.Button("TR")) Global.ChangeKeyLayout(new TR_StandardQ());
            if (imGui.Button("FR")) Global.ChangeKeyLayout(new FR_Standard());
            imGui.Column();
            imGui.Label("Screen Size");
            foreach (var mode in imGui.Canvas.AvailableModes)
            {
                if (imGui.Button(mode.AsString()))
                    Kernel.Instance.SetGraphicsMode(mode);
            }
            imGui.ResetColumn();
            if (imGui.Button("OK"))
            {
                ColorConfs.Background.Color = _backdropColor;
                ColorConfs.Text.Color = _textColor;
                ColorConfs.Component.Color = _backgroundColor;
                ColorConfs.Accent.Color = _hoverColor;
                WindowManager.Padding = _padding;
                MouseManager.MouseSensitivity = _sensitivity;
                WindowManager.ForceUpdate();
                WindowManager.Close(Id);
            }
        }
    }
}
