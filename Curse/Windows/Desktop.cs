﻿using Cosmos.System;
using Curse.Abstract;
using Curse.ImGUI;

namespace Curse.Windows
{
    class Desktop : Window
    {
        MouseLockProvider _startMenu;
        Category _currentCategory = Category.Apps;
        public override void Close()
        {
            _startMenu.Dispose();
        }

        public override string GetTitle() => nameof(Desktop);

        public override void Init()
        {
            Movable = false;
            Y = 0;
            //Topmost = true;
            _startMenu = new MouseLockProvider(WindowManager);
        }

        public override void DrawDecorations(ImGuiProvider imGui, int titlebarSize)
        {
        }

        public override void Update(ImGuiProvider imGui)
        {
            imGui.IgnoreLock();
            imGui.StartRow();
            if (imGui.Button("Start"))
            {
                "Start".PrintDebug();
                _startMenu.Locked = !_startMenu.Locked;
            }

            if (imGui.Button("Shutdown"))
                Power.Shutdown();
            imGui.EndRow();
            imGui.ResetColumn();
            if (_startMenu.Locked)
            {
                if (imGui.Button("Apps", hoverActivate: true))
                    _currentCategory = Category.Apps;
                if (imGui.Button("Utils", hoverActivate: true))
                    _currentCategory = Category.Utils;
                imGui.Column();
                if (_currentCategory == Category.Apps && imGui.Button(nameof(TestWindow)))
                {
                    WindowManager.Show(new TestWindow());
                    _startMenu.Locked = false;
                }
                if (_currentCategory == Category.Apps && imGui.Button(nameof(Files)))
                {
                    WindowManager.Show(new Files());
                    _startMenu.Locked = false;
                }
                if (_currentCategory == Category.Apps && imGui.Button(nameof(Zork)))
                {
                    WindowManager.Show(new Zork());
                    _startMenu.Locked = false;
                }
                if (_currentCategory == Category.Utils && imGui.Button(nameof(Configs)))
                {
                    WindowManager.Show(new Configs());
                    _startMenu.Locked = false;
                }
                if (_currentCategory == Category.Utils && imGui.Button("Close"))
                {
                    _startMenu.Locked = false;
                }

                if (!WindowManager.ContainsMouse(0, 0, Width, Height) && MouseState.Left.Clicking())
                    _startMenu.Locked = false;
            }
        }

        enum Category
        {
            Apps,
            Utils
        }
    }
}
