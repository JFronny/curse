﻿using System.Collections.Generic;
using System.IO;

namespace Curse.VFS
{
    public interface IFileSystem
    {
        public IEnumerable<FsPath> GetEntries(FsPath path);
        bool Exists(FsPath path);
        void CreateEmpty(FsPath path);
        Stream OpenFile(FsPath path);
        void Delete(FsPath path);
        bool ReadOnly(FsPath path);
    }
}
