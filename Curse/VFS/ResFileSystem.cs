﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Curse.VFS
{
    public class ResFileSystem : IFileSystem
    {
        private readonly Dictionary<string, FsPath> _paths = new Dictionary<string, FsPath>();
        private readonly Dictionary<string, byte[]> _data = new Dictionary<string, byte[]>();

        public IEnumerable<FsPath> GetEntries(FsPath path)
        {
            if (!path.IsDirectory)
                throw new DirectoryNotFoundException();
            List<FsPath> paths = new List<FsPath>();
            foreach (FsPath key in _paths.Values)
            {
                if (key.ParentPath == path)
                    paths.Add(key);
                else if (path.IsParentOf(key))
                {
                    FsPath p = key;
                    while (p.ParentPath != path)
                        p = p.ParentPath;
                    paths.Add(p);
                }
            }

            return paths;
        }

        public bool Exists(FsPath path) => _paths.ContainsValue(path);
        public void CreateEmpty(FsPath path) => throw new System.NotImplementedException();

        public Stream OpenFile(FsPath path)
        {
            foreach (var (key, value) in _paths)
            {
                if (value == path)
                    return new MemoryStream(_data[key]);
            }
            throw new FileNotFoundException();
        }

        public void Delete(FsPath path) => throw new System.NotImplementedException();

        public bool ReadOnly(FsPath path) => true;

        public ResFileSystem(Dictionary<string, byte[]> resources, FsPath rootPath)
        {
            try
            {
                foreach (var (file, data) in resources)
                {
                    FsPath fp = rootPath;
                    var ses = file.Split('/');
                    for (var i = 0; i < ses.Length; i++)
                    {
                        string s = ses[i];
                        fp = i == ses.Length - 1 ? rootPath.AppendFile(s) : rootPath.AppendDirectory(s);
                    }
                    
                    _paths.Add(file, fp);
                    _data.Add(file, data);
                }
            }
            catch (Exception e)
            {
                e.ToString().PrintDebug();
            }
        }
    }
}
