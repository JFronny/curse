﻿using System.Collections.Generic;
using System.IO;

namespace Curse.VFS
{
    public class MountFileSystem : IFileSystem
    {
        private readonly FsPath _root;
        public readonly Dictionary<string, IFileSystem> Mounts = new Dictionary<string, IFileSystem>();
        public IEnumerable<FsPath> GetEntries(FsPath path)
        {
            if (path == _root)
            {
                List<FsPath> paths = new List<FsPath>();
                foreach (var (key, _) in Mounts)
                {
                    FsPath i = FsPath.Parse(key);
                    if (i.ParentPath == path)
                        paths.Add(i);
                }
                return paths;
            }
            return GetFs(path).GetEntries(path);
        }

        public bool Exists(FsPath path) => path == _root || Mounts.ContainsKey(path.Path) || GetFs(path).Exists(path);
        public void CreateEmpty(FsPath path) => GetFs(path).CreateEmpty(path);
        public Stream OpenFile(FsPath path) => GetFs(path).OpenFile(path);
        public void Delete(FsPath path) => GetFs(path).Delete(path);
        public bool ReadOnly(FsPath path) => path == _root || GetFs(path).ReadOnly(path);
        private IFileSystem GetFs(FsPath path)
        {
            foreach (KeyValuePair<string, IFileSystem> pair in Mounts)
                if (pair.Key == path.Path || FsPath.Parse(pair.Key).IsParentOf(path))
                    return pair.Value;
            return new NullFs();
        }

        public MountFileSystem(FsPath root)
        {
            _root = root;
        }
    }
}