﻿using System.Collections.Generic;
using System.IO;

namespace Curse.VFS
{
    public class NullFs : IFileSystem
    {
        public IEnumerable<FsPath> GetEntries(FsPath path) => new FsPath[0];
        public bool Exists(FsPath path) => false;
        public void CreateEmpty(FsPath path) => throw new FileNotFoundException();
        Stream IFileSystem.OpenFile(FsPath path) => throw new System.NotImplementedException();
        public void Delete(FsPath path) => throw new FileNotFoundException();
        public bool ReadOnly(FsPath path) => true;
    }
}