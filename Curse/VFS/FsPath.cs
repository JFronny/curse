﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Curse.VFS
{
    public class FsPath : IEquatable<FsPath>, IComparable<FsPath>
    {
        public static readonly char DirectorySeparator = '/';
        public static FsPath Root { get; } = new FsPath(DirectorySeparator.ToString());
        public static FsPath Mnt { get; } = Root.AppendDirectory("mnt");
        public static FsPath Res { get; } = Root.AppendDirectory("res");

        private readonly string _path;
        
        public string Path => _path ?? "/";

        public bool IsDirectory => Path[Path.Length - 1] == DirectorySeparator;

        public bool IsFile => !IsDirectory;

        public bool IsRoot => Path.Length == 1;

        public string EntityName
        {
            get
            {
                string name = Path;
                if (IsRoot)
                    return null;
                int endOfName = name.Length;
                if (IsDirectory)
                    endOfName--;
                int startOfName = name.LastIndexOf(DirectorySeparator, endOfName - 1, endOfName) + 1;
                return name.Substring(startOfName, endOfName - startOfName);
            }
        }

        public FsPath ParentPath
        {
            get
            {
                string parentPath = Path;
                if (IsRoot)
                    throw new InvalidOperationException("There is no parent of root.");
                int lookaheadCount = parentPath.Length;
                if (IsDirectory)
                    lookaheadCount--;
                int index = parentPath.LastIndexOf(DirectorySeparator, lookaheadCount - 1, lookaheadCount);
                parentPath = parentPath.Remove(index + 1);
                return new FsPath(parentPath);
            }
        }

        private FsPath(string path) => _path = path;

        public static bool IsRooted(string s)
        {
            if (s.Length == 0)
                return false;
            return s[0] == DirectorySeparator;
        }

        public static FsPath Parse(string s)
        {
            if (s == null)
                throw new ArgumentNullException(nameof(s));
            if (!IsRooted(s))
                throw new ArgumentException("Path is not rooted", nameof(s));
            if (s.Contains(string.Concat(DirectorySeparator, DirectorySeparator)))
                throw new ArgumentException("Path contains double directory-separators.", s);
            return new FsPath(s);
        }

        public FsPath AppendPath(string relativePath)
        {
            if (IsRooted(relativePath))
                throw new ArgumentException("The specified path should be relative.", "relativePath");
            if (!IsDirectory)
                throw new InvalidOperationException("This FileSystemPath is not a directory.");
            return new FsPath(Path + relativePath);
        }
        
        public FsPath AppendPath(FsPath path)
        {
            if (!IsDirectory)
                throw new InvalidOperationException("This FileSystemPath is not a directory.");
            return new FsPath(Path + path.Path.Substring(1));
        }
        
        public FsPath AppendDirectory(string directoryName)
        {
            if (directoryName.Contains(DirectorySeparator.ToString()))
                throw new ArgumentException("The specified name includes directory-separator(s).", "directoryName");
            if (!IsDirectory)
                throw new InvalidOperationException("The specified FileSystemPath is not a directory.");
            return new FsPath(Path + directoryName + DirectorySeparator);
        }
        
        public FsPath AppendFile(string fileName)
        {
            if (fileName.Contains(DirectorySeparator.ToString()))
                throw new ArgumentException("The specified name includes directory-separator(s).", "fileName");
            if (!IsDirectory)
                throw new InvalidOperationException("The specified FileSystemPath is not a directory.");
            return new FsPath(Path + fileName);
        }
        
        public bool IsParentOf(FsPath path) => IsDirectory && Path.Length != path.Path.Length && path.Path.StartsWith(Path);
        
        public bool IsChildOf(FsPath path) => path.IsParentOf(this);
        
        public FsPath RemoveParent(FsPath parent)
        {
            if (!parent.IsDirectory)
                throw new ArgumentException("The specified path can not be the parent of this path: it is not a directory.");
            if (!Path.StartsWith(parent.Path))
                throw new ArgumentException("The specified path is not a parent of this path.");
            return new FsPath(Path.Remove(0, parent.Path.Length - 1));
        }

        public FsPath RemoveChild(FsPath child)
        {
            if (!Path.EndsWith(child.Path))
                throw new ArgumentException("The specified path is not a child of this path.");
            return new FsPath(Path.Substring(0, Path.Length - child.Path.Length + 1));
        }
        
        public string GetExtension()
        {
            if (!IsFile)
                throw new ArgumentException("The specified FileSystemPath is not a file.");
            string name = EntityName;
            int extensionIndex = name.LastIndexOf('.');
            if (extensionIndex < 0)
                return "";
            return name.Substring(extensionIndex);
        }
        
        public FsPath ChangeExtension(string extension)
        {
            if (!IsFile)
                throw new ArgumentException("The specified FileSystemPath is not a file.");
            string name = EntityName;
            int extensionIndex = name.LastIndexOf('.');
            if (extensionIndex >= 0)
                return ParentPath.AppendFile(name.Substring(0, extensionIndex) + extension);
            return FsPath.Parse(Path + extension);
        }
        
        public string[] GetDirectorySegments()
        {
            FsPath path = this;
            if (IsFile)
                path = path.ParentPath;
            var segments = new LinkedList<string>();
            while(!path.IsRoot)
            {
                segments.AddFirst(path.EntityName);
                path = path.ParentPath;
            }
            return segments.ToArray();
        }
        
        public int CompareTo(FsPath other) => Path.CompareTo(other.Path);
        
        public override string ToString() => Path;
        
        public override bool Equals(object obj)
        {
            if (obj is FsPath path)
                return Equals(path);
            return false;
        }
        
        public bool Equals(FsPath other) => other is { } && other.Path.Equals(Path);

        public override int GetHashCode() => Path.GetHashCode();

        public static bool operator ==(FsPath pathA, FsPath pathB) => pathA?.Equals(pathB) ?? pathB is null;

        public static bool operator !=(FsPath pathA, FsPath pathB) => !(pathA == pathB);
    }
}