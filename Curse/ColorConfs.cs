﻿using System.Drawing;
using Cosmos.System.Graphics;

namespace Curse
{
    public static class ColorConfs
    {
        public static Pen Background = new Pen(Color.White);
        public static Pen Text = new Pen(Color.Black);
        public static Pen Component = new Pen(Color.Orange);
        public static Pen Accent = new Pen(Color.OrangeRed);
    }
}