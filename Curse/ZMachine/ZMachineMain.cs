using Curse.Windows;
using Curse.ZMachine.Machine;

namespace Curse.ZMachine
{
    public class ZMachineMain
    {
        public ZHeader Header { get; }

        public IZInput Input { get; }
        public IZOutput Output { get; }
        public ZMemory Memory { get; }
        private Zork Screen { get; }

        private ZInterpreter MInterpreter { get; set; }

        public ZMachineMain(byte[] aData, Zork screen)
        {
            Memory = new ZMemory(aData);
            Header = new ZHeader(Memory);

            Screen = screen;
            Input = new ZInput(Header, Screen, Memory);
            Output = new ZOutput(Screen);
        }

        public void Run()
        {
            Memory.Initialize(Header.StartPc);
            MInterpreter = new ZInterpreter(this);

            ZText.Initialize(this);

            MInterpreter.Interpret();
        }
    }
}
