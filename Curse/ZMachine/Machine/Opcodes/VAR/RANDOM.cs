﻿namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    public class Random : Opcode
    {
        public Random(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "VAR:0x07 random range -> (result)";
        }

        protected override void Execute(ushort aRange, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            if ((short)aRange <= 0)
            {
                ZRandom.Seed(-(short)aRange);
                Store(0);

            }
            else
            {
                ushort result;

                if (ZRandom.Interval != 0)
                {
                    result = (ushort)ZRandom.Counter++;
                    if (ZRandom.Counter == ZRandom.Interval)
                    {
                        ZRandom.Counter = 0;
                    }
                }
                else
                {
                    ZRandom.A = 0x015a4e35 * ZRandom.A + 1;
                    result = (ushort)((ZRandom.A >> 16) & 0x7fff);
                }

                Store((ushort)(result % aRange + 1));
            }
        }
    }
}
