﻿using System;

namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    public class CallVn2 : Opcode
    {
        public CallVn2(ZMachineMain machine)
            : base(machine)
        {
            Name = "VAR:0x1A call_vn2 routine ...up to 7 args...";
        }

        protected override void Execute(ushort aArg0, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            throw new NotImplementedException();
        }
    }
}
