﻿using System;

namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    public class ReadChar : Opcode
    {
        public ReadChar(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "VAR:0x16 read_char 1 time routine -> (result)";
        }

        protected override void Execute(ushort aArg0, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            throw new NotImplementedException();
        }
    }
}
