﻿using System;

namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    public class SetCursor : Opcode
    {
        public SetCursor(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "VAR:0x0F set_cursor line column, set_cursor line column window";
        }

        protected override void Execute(ushort aArg0, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            throw new NotImplementedException();
        }
    }
}
