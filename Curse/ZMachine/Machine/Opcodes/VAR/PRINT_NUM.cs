﻿namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    public class PrintNum : Opcode
    {
        public PrintNum(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "VAR:0x06 print_num value";
        }

        protected override void Execute(ushort aValue, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            Machine.Output.PrintZscii((short) aValue);
        }
    }
}
