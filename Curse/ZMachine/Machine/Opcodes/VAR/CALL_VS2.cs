﻿using System;

namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    public class CallVs2 : Opcode
    {
        public CallVs2(ZMachineMain machine)
            : base(machine)
        {
            Name = "VAR:0x0C call_vs2 routine...0 to 7 args... -> (result)";
        }

        protected override void Execute(ushort aArg0, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            throw new NotImplementedException();
        }
    }
}
