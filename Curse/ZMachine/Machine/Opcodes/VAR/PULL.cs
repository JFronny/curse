﻿namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    /// <summary>
    /// Pop a value off the game stack and
    /// store it at address (V6)
    /// store it in a variable (!V6) 
    /// </summary>
    public class Pull : Opcode
    {
        public Pull(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "VAR:0x09 pull(variable), pull stack -> (result)";
        }

        protected override void Execute(ushort aAddressOrVariable, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            ushort xValue = Machine.Memory.Stack.Pop();

            if (aAddressOrVariable == 0)
            {
                Machine.Memory.Stack[Machine.Memory.Stack.Sp] = xValue;
            }
            else if (aAddressOrVariable < 16)
            {
                Machine.Memory.Stack[Machine.Memory.Stack.Bp - aAddressOrVariable] = xValue;
            }
            else
            {
                ushort xAddress = (ushort) (Machine.Header.GlobalsOffset + 2 * (aAddressOrVariable - 16));
                Machine.Memory.SetWord(xAddress, xValue);
            }
        }
    }
}
