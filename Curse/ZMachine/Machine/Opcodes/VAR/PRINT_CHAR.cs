﻿namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    /// <summary>
    /// Print a single zscii character.
    /// </summary>
    public class PrintChar : Opcode
    {
        public PrintChar(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "VAR:0x05 print_char output-character-code";
        }

        protected override void Execute(ushort aChar, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            Machine.Output.PrintZscii((short) aChar);
        }
    }
}
