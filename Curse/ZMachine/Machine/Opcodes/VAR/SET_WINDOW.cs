﻿using System;

namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    public class SetWindow : Opcode
    {
        public SetWindow(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "VAR:0x0B set_window window";
        }

        protected override void Execute(ushort aArg0, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            throw new NotImplementedException();
        }
    }
}
