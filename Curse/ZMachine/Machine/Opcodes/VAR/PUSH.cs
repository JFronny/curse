﻿namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    /// <summary>
    /// Push a value onto game the stack.
    /// </summary>
    public class Push : Opcode
    {
        public Push(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "VAR:0x08 push value";
        }

        protected override void Execute(ushort aValue, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            Machine.Memory.Stack.Push(aValue);
        }
    }
}
