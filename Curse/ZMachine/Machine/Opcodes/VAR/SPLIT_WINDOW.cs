﻿using System;

namespace Curse.ZMachine.Machine.Opcodes.VAR
{
    public class SplitWindow : Opcode
    {
        public SplitWindow(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "VAR:0x0A split_window lines";
        }

        protected override void Execute(ushort aArg0, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            throw new NotImplementedException();
        }
    }
}
