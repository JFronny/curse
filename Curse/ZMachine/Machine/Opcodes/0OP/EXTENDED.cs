﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class Extended : Opcode
    {
        public Extended(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x0E [first byte of extended opcode]";
        }

        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
