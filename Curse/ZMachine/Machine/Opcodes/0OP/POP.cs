﻿namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class Pop : Opcode
    {
        public Pop(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x09 pop";
        }

        public override void Execute()
        {
            Machine.Memory.Stack.Pop();
        }
    }
}
