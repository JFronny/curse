﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class Piracy : Opcode
    {
        public Piracy(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x0F piracy? (label)";
        }

        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
