﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class Quit : Opcode
    {
        public Quit(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x0A quit";
        }

        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
