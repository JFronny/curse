﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class Nop : Opcode
    {
        public Nop(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x04 nop";
        }

        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
