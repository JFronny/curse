﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class ShowStatus : Opcode
    {
        public ShowStatus(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x0C show_status";
        }

        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
