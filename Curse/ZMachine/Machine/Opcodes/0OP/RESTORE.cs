﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class Restore : Opcode
    {
        public Restore(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x06 restore? (label), restore -> (result)";
        }

        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
