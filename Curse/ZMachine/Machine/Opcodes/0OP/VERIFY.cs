﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class Verify : Opcode
    {
        public Verify(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x0D verify? (label)";
        }

        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
