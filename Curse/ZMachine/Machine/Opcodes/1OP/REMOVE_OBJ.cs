﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    public class RemoveObj : Opcode
    {
        public RemoveObj(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP: 0x09 remove_obj object";
        }

        public override void Execute(ushort aObject)
        {
            throw new NotImplementedException();
        }
    }
}
