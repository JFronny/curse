﻿namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    /// <summary>
    /// Store the sibling of an object.
    /// </summary>
    public class GetSibling : Opcode
    {
        public GetSibling(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP:0x01 get_sibling object -> (result)? (label)";
        }

        public override void Execute(ushort aObject)
        {
            ushort objAddr;

            if (aObject == 0)
            {
                Store(0);
                Branch(false);
                return;
            }

            objAddr = Machine.GetObjectAddress(aObject);

            if (Machine.Header.Version <= 3)
            {
                objAddr += ZObject.O1Sibling;
                Machine.Memory.GetByte(objAddr, out var sibling);

                Store(sibling);
                Branch(sibling > 0);
            }
            else
            {
                objAddr += ZObject.O4Sibling;
                Machine.Memory.GetWord(objAddr, out var sibling);

                Store(sibling);
                Branch(sibling > 0);
            }
        }
    }
}
