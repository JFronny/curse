﻿namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    /// <summary>
    /// Store the parent of an object.
    /// </summary>
    public class GetParent : Opcode
    {
        public GetParent(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP:0x03 get_parent object -> (result)";
        }

        public override void Execute(ushort aObject)
        {
            if (aObject == 0)
            {
                Store(0);
                return;
            }

            ushort objAddress = Machine.GetObjectAddress(aObject);

            if (Machine.Header.Version <= 3)
            {
                objAddress += ZObject.O1Parent;
                Machine.Memory.GetByte(objAddress, out byte parent);
                Store(parent);
            }
            else
            {
                objAddress += ZObject.O4Parent;
                Machine.Memory.GetWord(objAddress, out ushort parent);
                Store(parent);
            }
        }
    }
}
