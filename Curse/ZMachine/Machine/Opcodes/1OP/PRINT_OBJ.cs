﻿namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    /// <summary>
    /// Print an object description.
    /// </summary>
    public class PrintObj : Opcode
    {
        public PrintObj(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP:0x0A print_obj object";
        }

        public override void Execute(ushort aObject)
        {
            string s = Machine.GetObjectName(aObject);
            Machine.Output.PrintString(s);
        }
    }
}
