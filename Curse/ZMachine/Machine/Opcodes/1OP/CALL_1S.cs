﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    public class Call1S : Opcode
    {
        public Call1S(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP: 0x08 call_1s routine -> (result)";
        }

        public override void Execute(ushort aRoutineAddress)
        {
            throw new NotImplementedException();
        }
    }
}
