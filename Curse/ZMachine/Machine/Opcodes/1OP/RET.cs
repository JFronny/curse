﻿namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    public class Ret : Opcode
    {
        public Ret(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP:0x0B ret value";
        }

        public override void Execute(ushort aValue)
        {
            Return(aValue);
        }
    }
}
