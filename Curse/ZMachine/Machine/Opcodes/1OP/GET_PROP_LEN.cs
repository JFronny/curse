﻿namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    public class GetPropLen : Opcode
    {
        public GetPropLen(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP:0x04 get_prop_len property-address -> (result)";
        }

        public override void Execute(ushort aPropertyAddress)
        {
            ushort addr;

            /* Back up the property pointer to the property id */

            addr = (ushort)(aPropertyAddress - 1);
            Machine.Memory.GetByte(addr, out var value);

            /* Calculate length of property */

            if (Machine.Header.Version <= 3)
            {
                value = (byte)((value >> 5) + 1);
            }
            else if (!((value & 0x80) > 0))
            {
                value = (byte)((value >> 6) + 1);
            }
            else
            {

                value &= 0x3f;

                if (value == 0)
                {
                    value = 64; /* demanded by Spec 1.0 */
                }
            }

            /* Store length of property */

            Store(value);
        }
    }
}
