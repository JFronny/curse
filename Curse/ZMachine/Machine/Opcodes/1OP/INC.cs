﻿namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    /// <summary>
    /// Increment a variable.
    /// </summary>
    public class Inc : Opcode
    {
        public Inc(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP: 0x05 inc(variable)";
        }

        public override void Execute(ushort aVariable)
        {
            if (aVariable == 0)
            {
                Machine.Memory.Stack[Machine.Memory.Stack.Sp]++;
            }
            else if (aVariable < 16)
            {
                (Machine.Memory.Stack[Machine.Memory.Stack.Bp - aVariable])++;
            }
            else
            {
                ushort addr = (ushort)(Machine.Header.GlobalsOffset + 2 * (aVariable - 16));
                Machine.Memory.GetWord(addr, out var value);
                value++;
                Machine.Memory.SetWord(addr, value);
            }
        }
    }
}
