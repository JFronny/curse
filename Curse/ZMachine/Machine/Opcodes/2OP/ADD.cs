﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    /// <summary>
    /// 16-bit ADD
    /// </summary>
    public class Add : Opcode
    {
        public Add(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x14 add a b -> (result)";
        }

        public override void Execute(ushort aValue1, ushort aValue2)
        {
            Store((ushort) ((short) aValue1 + (short) aValue2));
        }
    }
}
