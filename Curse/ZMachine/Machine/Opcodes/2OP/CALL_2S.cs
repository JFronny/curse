﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    public class Call2S : Opcode
    {
        public Call2S(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x19 call_2s routine arg1 -> (result)";
        }

        public override void Execute(ushort aRoutineAddress, ushort aArg)
        {
            throw new NotImplementedException();
        }
    }
}
