﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    public class GetNextProp : Opcode
    {
        public GetNextProp(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x13 get_next_prop object property -> (result)";
        }

        public override void Execute(ushort aObject, ushort aProperty)
        {
            throw new NotImplementedException();
        }
    }
}
