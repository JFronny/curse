﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    /// <summary>
    /// Read a value from a table of words.
    /// </summary>
    public class Loadw : Opcode
    {
        public Loadw(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x0F loadw array word-index -> (result)";
        }

        public override void Execute(ushort aTableAddress, ushort aEntryIndex)
        {
            ushort addr = (ushort)(aTableAddress + 2 * aEntryIndex);

            Machine.Memory.GetWord(addr, out var value);

            Store(value);
        }
    }
}
