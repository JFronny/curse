﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    public class Mul : Opcode
    {
        public Mul(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x16 mul a b -> (result)";
        }

        public override void Execute(ushort aValue1, ushort aValue2)
        {
            Store((ushort) (aValue1 * aValue2));
        }
    }
}
