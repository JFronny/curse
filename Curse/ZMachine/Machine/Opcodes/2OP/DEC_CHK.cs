﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    /// <summary>
    /// Decrement a variable and branch if less than a value.
    /// </summary>
    public class DecChk : Opcode
    {
        public DecChk(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x04 DEC_CHK (variable) value ?(label)";
        }

        public override void Execute(ushort aVariable, ushort aValue)
        {
            ushort v;

            if (aVariable == 0)
            {
                v = --(Machine.Memory.Stack[Machine.Memory.Stack.Sp]);
            }
            else if (aVariable < 16)
            {
                v = --(Machine.Memory.Stack[Machine.Memory.Stack.Bp - aVariable]);
            }
            else
            {
                ushort xAddress = (ushort)(Machine.Header.GlobalsOffset + 2 * (aVariable - 16));
                Machine.Memory.GetWord(xAddress, out v);
                v--;
                Machine.Memory.SetWord(xAddress, v);
            }

            Branch((short)v < (short)aValue);
        }
    }
}
