﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    /// <summary>
    /// Exit game because an unknown opcode has been hit.
    /// </summary>
    public class Illegal : Opcode
    {
        public Illegal(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "ILLEGAL";
        }

        public override void Execute(ushort aArg0, ushort aArg1)
        {

        }
    }
}
