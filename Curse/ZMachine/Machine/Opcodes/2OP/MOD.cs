﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    public class Mod : Opcode
    {
        public Mod(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x18 mod a b -> (result)";
        }

        public override void Execute(ushort aValue1, ushort aValue2)
        {
            throw new NotImplementedException();
        }
    }
}
