﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    /// <summary>
    /// Store the value of an object property.
    /// </summary>
    public class GetProp : Opcode
    {
        public GetProp(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x11 get_prop object property -> (result)";
        }

        public override void Execute(ushort aObject, ushort aProperty)
        {
            ushort propAddress;
            ushort wpropVal;
            byte value;
            byte mask;

            if (aObject == 0)
            {
                Store(0);
                return;
            }

            mask = (byte)((Machine.Header.Version <= (byte) 3) ? 0x1f : 0x3f);

            propAddress = Machine.GetFirstProperty(aObject);

            for (; ; )
            {
                Machine.Memory.GetByte(propAddress, out value);
                if ((value & mask) <= aProperty)
                {
                    break;
                }
                propAddress = Machine.GetNextProperty(propAddress);
            }

            if ((value & mask) == aProperty)
            {
                propAddress++;

                if ((Machine.Header.Version <= 3 && !((value & 0xe0) > 0)) ||
                    (Machine.Header.Version >= 4 && !((value & 0xc0) > 0)))
                {

                    Machine.Memory.GetByte(propAddress, out var bpropVal);
                    wpropVal = bpropVal;

                }
                else
                {
                    Machine.Memory.GetWord(propAddress, out wpropVal);
                }
            }
            else
            {
                propAddress = (ushort)(Machine.Header.ObjectsOffset + 2 * (aProperty - 1));
                Machine.Memory.GetWord(propAddress, out wpropVal);
            }

            Store(wpropVal);
        }
    }
}
