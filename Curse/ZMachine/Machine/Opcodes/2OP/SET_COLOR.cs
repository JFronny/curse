﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    public class SetColor : Opcode
    {
        public SetColor(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x1B set_colour foreground background";
        }

        public override void Execute(ushort aForeground, ushort aBackground)
        {
            throw new NotImplementedException();
        }
    }
}
