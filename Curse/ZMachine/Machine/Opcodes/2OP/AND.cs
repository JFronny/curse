﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    /// <summary>
    /// Bitwise AND
    /// </summary>
    public class And : Opcode
    {
        public And(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x09 AND a b -> (result)";
        }

        public override void Execute(ushort aValue1, ushort aValue2)
        {
            Store((ushort) (aValue1 & aValue2));
        }

        protected override void Execute(ushort aValue1, ushort aValue2, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            Store((ushort)(aValue1 & aValue2));
        }
    }
}
