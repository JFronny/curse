﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    public class Test : Opcode
    {
        public Test(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x07 TEST bitmap flags ?(label)";
        }

        public override void Execute(ushort aBitmap, ushort aFlags)
        {
            Branch((aBitmap & aFlags) == aFlags);
        }
    }
}
