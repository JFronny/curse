﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    /// <summary>
    /// Read a value from a table of bytes.
    /// </summary>
    public class Loadb : Opcode
    {
        public Loadb(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x10 loadb array byte-index -> (result)";
        }

        public override void Execute(ushort aTableAddress, ushort aEntryIndex)
        {
            ushort addr = (ushort)(aTableAddress + aEntryIndex);

            Machine.Memory.GetByte(addr, out var value);

            Store(value);
        }
    }
}
