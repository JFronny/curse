﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    public class Call2N : Opcode
    {
        public Call2N(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x1A call_2s routine arg1";
        }

        public override void Execute(ushort aRoutineAddress, ushort aArg)
        {
            throw new NotImplementedException();
        }
    }
}
