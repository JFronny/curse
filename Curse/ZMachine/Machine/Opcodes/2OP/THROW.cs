﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    public class Throw : Opcode
    {
        public Throw(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:1C throw value stack-frame";
        }

        public override void Execute(ushort aValue, ushort aStackFrame)
        {
            throw new NotImplementedException();
        }
    }
}
