using System;
using System.Collections.Generic;
using System.Text;
using Curse.Windows;

namespace Curse.ZMachine.Machine
{
    public class ZInput : IZInput
    {
        private class Token
        {
            public readonly byte StartPos;
            public readonly byte Length;

            public Token(byte startPos, byte length)
            {
                StartPos = startPos;
                Length = length;
            }
        }

        private readonly ZHeader _header;

        private readonly Zork _screen;

        private readonly ZMemory _memory;

        public ZInput(ZHeader header, Zork aScreen, ZMemory aMemory)
        {
            _header = header;
            _screen = aScreen;
            _memory = aMemory;
        }

        public void Read(ushort aCharacterBufferAddress, ushort aTokenBufferAddress, ushort aTimeout, ushort aTimeoutCallback, Action callback)
        {
            byte initlen;

            _memory.GetByte(aCharacterBufferAddress, out byte max);
            if (_header.Version <= 4)
            {
                max--;
            }

            if (max >= 200)
            {
                max = 200 - 1;
            }

            if (_header.Version >= 5)
            {
                aCharacterBufferAddress++;
                _memory.GetByte(aCharacterBufferAddress, out initlen);
            }
            else
            {
                initlen = 0;
            }

            string initial = string.Empty;
            if (initlen > 0)
            {
                // we never get here for V1-4
                StringBuilder sb = new StringBuilder(initlen);
                for (int i = 0; i < initlen; i++)
                {
                    _memory.GetByte((aCharacterBufferAddress + 2 + i), out byte value);
                    sb.Append(ZText.CharFromZscii(value));
                }
                initial = sb.ToString();
            }
            
            _screen.ReadLine(initial, aTimeout, aTimeoutCallback, ZText.TerminatingChars, (s, _) =>
            {
                byte[] chars = ZText.StringToZscii(s.ToLower());
                if (_header.Version <= 4)
                {
                    for (int i = 0; i < Math.Min(chars.Length, max); i++)
                    {
                        _memory.SetByte((aCharacterBufferAddress + 1 + i), chars[i]);
                    }
                    _memory.SetByte((aCharacterBufferAddress + 1 + Math.Min(chars.Length, max)), 0);
                }
                else
                {
                    _memory.SetByte((aCharacterBufferAddress + 1), (byte)chars.Length);
                    for (int i = 0; i < Math.Min(chars.Length, max); i++)
                    {
                        _memory.SetByte((aCharacterBufferAddress + 2 + i), chars[i]);
                    }
                }

                if (aTokenBufferAddress != 0)
                {
                    Tokenize(aCharacterBufferAddress, aTokenBufferAddress, 0, false);
                }

                callback();
            });
        }

        private bool IsWhitespace(byte aChar)
        {
            return (aChar == 9) || (aChar == 32);
        }

        private bool IsWordSeparator(byte aChar, ushort aUserDictionaryAddress)
        {
            byte[] seps;

            if (aUserDictionaryAddress == 0)
            {
                seps = ZText.WordSeparators;
            }
            else
            {
                _memory.GetByte(aUserDictionaryAddress, out byte n);
                _memory.GetBytes(aUserDictionaryAddress + 1, n, out seps);
            }

            return (Array.IndexOf(seps, aChar) >= 0);
        }

        private List<Token> SplitTokens(byte[] aBuffer, ushort aUserDictionaryAddress)
        {
            var result = new List<Token>();

            int xStart = -1;

            for (int i = 0; i < aBuffer.Length; i++)
            {
                // End of string
                if (aBuffer[i] == 0 && xStart >=0)
                {
                    result.Add(new Token((byte)xStart, (byte)(i - xStart)));
                    break;
                }

                // Skip whitespace
                if (IsWhitespace(aBuffer[i]) && xStart <0)
                {
                    continue;
                }

                if (IsWordSeparator(aBuffer[i], aUserDictionaryAddress))
                {
                    result.Add(new Token((byte)i, 1));
                    xStart = -1;
                }
                else
                {
                    if (xStart < 0)
                    {
                        xStart = i;
                    }

                    // find the end of the word
                    if (i < aBuffer.Length && !IsWhitespace(aBuffer[i]) && !IsWordSeparator(aBuffer[i], aUserDictionaryAddress))
                    {
                        continue;
                    }

                    // add it to the list
                    result.Add(new Token((byte) xStart, (byte)(i - xStart)));
                    xStart = -1;
                }
            }

            return result;
        }

        private void Tokenize(ushort aCharacterBufferAddress, ushort aTokenBufferAddress, ushort aUserDictionaryAddress, bool aSkipUnrecognized)
        {
            byte bufLen;
            int tokenOffset;

            _memory.SetByte(aTokenBufferAddress + 1, 0);

            if (_header.Version >= 4)
            {
                bufLen = 0;
                tokenOffset = 1;

                for (int i = aCharacterBufferAddress + 1; i < _header.DynamicSize; i++)
                {
                    _memory.GetByte(i, out byte value);
                    if (value == 0)
                    {
                        bufLen = (byte)(i - aCharacterBufferAddress - 1);
                        break;
                    }
                }
            }
            else
            {
                _memory.GetByte((aCharacterBufferAddress + 1), out bufLen);
                tokenOffset = 1;
            }

            _memory.GetByte((aTokenBufferAddress + 0), out byte max);
            byte count = 0;

            _memory.GetBytes(aCharacterBufferAddress + tokenOffset, bufLen, out byte[] myBuffer);
            List<Token> tokens = SplitTokens(myBuffer, aUserDictionaryAddress);

            foreach (Token tok in tokens)
            {
                ushort word = LookUpWord(myBuffer, tok.StartPos, tok.Length, aUserDictionaryAddress);
                if (word == 0 && aSkipUnrecognized)
                {
                    continue;
                }

                _memory.SetByte((aTokenBufferAddress + 1), (byte)(count + 1));
                _memory.SetByte((aTokenBufferAddress + tokenOffset + 4 * count + 1), (byte) (word >> 8));
                _memory.SetByte((aTokenBufferAddress + tokenOffset + 4 * count + 2), (byte) (word & 0xff));
                _memory.SetByte((aTokenBufferAddress + tokenOffset + 4 * count + 3), tok.Length);
                _memory.SetByte((aTokenBufferAddress + tokenOffset + 4 * count + 4), (byte)(tokenOffset + tok.StartPos));
                count++;

                if (count == max)
                {
                    break;
                }
            }
        }

        private ushort LookUpWord(byte[] aBuffer, int aPosition, int aLength, ushort aUserDictionaryAddress)
        {
            int dictStart;

            byte[] word = ZText.EncodeText(aBuffer, aPosition, aLength, ZText.DictionaryWordSize);

            if (aUserDictionaryAddress != 0)
            {
                _memory.GetByte(aUserDictionaryAddress, out byte n);
                dictStart = aUserDictionaryAddress + 1 + n;
            }
            else
            {
                dictStart = _header.DictionaryOffset + 1 + ZText.WordSeparators.Length;
            }

            _memory.GetByte(dictStart++, out byte entryLength);

            ushort entries;
            if (aUserDictionaryAddress == 0)
            {
                _memory.GetWord(dictStart, out entries);
            }
            else
            {
                _memory.GetWord(dictStart, out entries);
            }

            dictStart += 2;

            if ((short)entries < 0)
            {
                // use linear search for unsorted user dictionary
                for (int i = 0; i < entries; i++)
                {
                    int addr = dictStart + i * entryLength;
                    if (CompareWords(word, addr) == 0)
                    {
                        return (ushort)addr;
                    }
                }
            }
            else
            {
                // use binary search
                int start = 0, end = entries;
                while (start < end)
                {
                    int mid = (start + end) / 2;
                    int addr = dictStart + mid * entryLength;
                    int cmp = CompareWords(word, addr);
                    if (cmp == 0)
                    {
                        return (ushort)addr;
                    }

                    if (cmp < 0)
                    {
                        end = mid;
                    }
                    else
                    {
                        start = mid + 1;
                    }
                }
            }

            return 0;
        }

        private int CompareWords(byte[] aWord, int aAddress)
        {
            for (int i = 0; i < aWord.Length; i++)
            {
                _memory.GetByte(aAddress + i, out byte value);
                int cmp = aWord[i] - value;
                if (cmp != 0)
                {
                    return cmp;
                }
            }

            return 0;
        }
    }
}
