using Curse.ZMachine.Machine.Opcodes;

namespace Curse.ZMachine.Machine
{
    public class ZInterpreter
    {
        public int Finished;

        private Opcode[] _op0Opcodes;

        private Opcode[] _op1Opcodes;

        private Opcode[] _op2Opcodes;

        private Opcode[] _varOpcodes;

        private readonly ZMachineMain _machine;

        public ZInterpreter(ZMachineMain aMachine)
        {
            _machine = aMachine;
            Finished = 0;
            InitializeOpcodes();
        }

        private void InitializeOpcodes()
        {
            InitializeOp0Opcodes();
            InitializeOp1Opcodes();
            InitializeOp2Opcodes();
            InitializeVarOpcodes();
            InitializeExtOpcodes();
        }

        private void InitializeOp0Opcodes()
        {
           _op0Opcodes = new Opcode[]
            {
                new Opcodes._0OP.Rtrue(_machine), // 0x00
                new Opcodes._0OP.Rfalse(_machine), // 0x01
                new Opcodes._0OP.Print(_machine), // 0x02
                new Opcodes._0OP.PrintRet(_machine), // 0x03
                new Opcodes._0OP.Nop(_machine), // 0x04
                new Opcodes._0OP.Save(_machine), // 0x05
                new Opcodes._0OP.Restore(_machine), // 0x06
                new Opcodes._0OP.Restart(_machine), // 0x07
                new Opcodes._0OP.RetPopped(_machine), // 0x08
                new Opcodes._0OP.Pop(_machine), // 0x09
                new Opcodes._0OP.Quit(_machine), // 0x0A
                new Opcodes._0OP.NewLine(_machine), // 0x0B
                new Opcodes._0OP.ShowStatus(_machine), // 0x0C
                new Opcodes._0OP.Verify(_machine), // 0x0D
                new Opcodes._0OP.Extended(_machine), // 0x0E
                new Opcodes._0OP.Piracy(_machine) // 0x0F
            };
        }

        private void InitializeOp1Opcodes()
        {
            _op1Opcodes = new Opcode[]
            {
                new Opcodes._1OP.Jz(_machine), // 0x00
                new Opcodes._1OP.GetSibling(_machine), // 0x01
                new Opcodes._1OP.GetChild(_machine), // 0x02
                new Opcodes._1OP.GetParent(_machine), //0x03
                new Opcodes._1OP.GetPropLen(_machine), // 0x04
                new Opcodes._1OP.Inc(_machine), // 0x05
                new Opcodes._1OP.Dec(_machine), // 0x06
                new Opcodes._1OP.PrintAddr(_machine), // 0x07
                new Opcodes._1OP.Call1S(_machine), // 0x08
                new Opcodes._1OP.RemoveObj(_machine), // 0x09
                new Opcodes._1OP.PrintObj(_machine), // 0x0A
                new Opcodes._1OP.Ret(_machine), // 0x0B
                new Opcodes._1OP.Jump(_machine), // 0x0C
                new Opcodes._1OP.PrintPaddr(_machine), // 0x0D 
                new Opcodes._1OP.Load(_machine), // 0x0E
                new Opcodes._1OP.Not(_machine) // 0x0F
            };
        }

        private void InitializeOp2Opcodes()
        {
            _op2Opcodes = new Opcode[]
            {
                new Opcodes._2OP.Illegal(_machine), // 0x00
                new Opcodes.VAR.Je(_machine), // 0x01
                new Opcodes.VAR.Jl(_machine), // 0x02
                new Opcodes.VAR.Jg(_machine), // 0x03
                new Opcodes._2OP.DecChk(_machine), // 0x04
                new Opcodes._2OP.IncChk(_machine), // 0x05
                new Opcodes._2OP.Jin(_machine), // 0x06
                new Opcodes._2OP.Test(_machine), // 0x07
                new Opcodes._2OP.Or(_machine), // 0x08
                new Opcodes._2OP.And(_machine), // 0x09
                new Opcodes._2OP.TestAttr(_machine), // 0x0A
                new Opcodes._2OP.SetAttr(_machine), // 0x0B
                new Opcodes._2OP.ClearAttr(_machine), // 0x0C
                new Opcodes._2OP.Store(_machine), // 0x0D
                new Opcodes._2OP.InsertObj(_machine), // 0x0E
                new Opcodes._2OP.Loadw(_machine), // 0x0F
                new Opcodes._2OP.Loadb(_machine), // 0x10
                new Opcodes._2OP.GetProp(_machine), // 0x11
                new Opcodes._2OP.GetPropAddr(_machine), // 0x12
                new Opcodes._2OP.GetNextProp(_machine), // 0x13
                new Opcodes._2OP.Add(_machine), // 0x14
                new Opcodes._2OP.Sub(_machine), // 0x15
                new Opcodes._2OP.Mul(_machine), // 0x16
                new Opcodes._2OP.Div(_machine), // 0x17
                new Opcodes._2OP.Mod(_machine), // 0x18
                new Opcodes._2OP.Call2S(_machine), // 0x19
                new Opcodes._2OP.Call2N(_machine), // 0x1A
                new Opcodes._2OP.SetColor(_machine), // 0x1B
                new Opcodes._2OP.Throw(_machine), // 0x1C
                null, // 0x1F
                null, // 0x1E
                null // 0x1F
            };
        }

        private void InitializeVarOpcodes()
        {
            _varOpcodes = new Opcode[]
        {
            new Opcodes.VAR.Illegal(_machine),
            new Opcodes.VAR.Je(_machine),
            new Opcodes.VAR.Jl(_machine),
            new Opcodes.VAR.Jg(_machine),
            null,
            new Opcodes._2OP.IncChk(_machine),
            null,
            null,
            new Opcodes._2OP.Or(_machine),
            new Opcodes._2OP.And(_machine),
            null,
            null,
            null,
            new Opcodes._2OP.Store(_machine),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            new Opcodes.VAR.Call(_machine), // 0x20
            new Opcodes.VAR.Storew(_machine), // 0x21
            new Opcodes.VAR.Storeb(_machine), // 0x22
            new Opcodes.VAR.PutProp(_machine), // 0x23
            new Opcodes.VAR.Sread(_machine), // 0x24
            new Opcodes.VAR.PrintChar(_machine), // 0x25
            new Opcodes.VAR.PrintNum(_machine), // 0x26
            new Opcodes.VAR.Random(_machine), // 0x27
            new Opcodes.VAR.Push(_machine), // 0x28
            new Opcodes.VAR.Pull(_machine), // 0x29
            new Opcodes.VAR.SplitWindow(_machine), // 0x2A
            new Opcodes.VAR.SetWindow(_machine), // 0x2B
            new Opcodes.VAR.CallVs2(_machine), // 0x2C
            new Opcodes.VAR.EraseWindow(_machine), // 0x2D
            new Opcodes.VAR.EraseLine(_machine), // 0x2E
            new Opcodes.VAR.SetCursor(_machine), // 0x2F
            new Opcodes.VAR.GetCursor(_machine), // 0x30
            new Opcodes.VAR.SetTextStyle(_machine), // 0x31
            new Opcodes.VAR.BufferMode(_machine), // 0x32
            new Opcodes.VAR.OutputStream(_machine), // 0x33
            new Opcodes.VAR.InputStream(_machine), // 0x34
            null, // 0x35
            new Opcodes.VAR.ReadChar(_machine), // 0x36
            new Opcodes.VAR.ScanTable(_machine), // 0x37
            new Opcodes.VAR.Not(_machine), // 0x38
            new Opcodes.VAR.CallVn(_machine), // 0x39
            new Opcodes.VAR.CallVn2(_machine), // 0x3A
            new Opcodes.VAR.Tokenise(_machine), // 0x3B
            new Opcodes.VAR.EncodeText(_machine), // 0x3C
            new Opcodes.VAR.CopyTable(_machine), // 0x3D
            new Opcodes.VAR.PrintTable(_machine), // 0x3E
            new Opcodes.VAR.CheckArgCount(_machine), // 0x3F
        };
        }

        private void InitializeExtOpcodes()
        {
        }

        public void Interpret()
        {
            do
            {
                _machine.Memory.CodeByte(out byte opcode);
                ushort xArg0, xArg1, xArg4 = 0, xArg5 = 0, xArg6 = 0 , xArg7 = 0;
                if (opcode < 0x80) // 2OP Opcodes
                {
                    LoadOperand((byte)((opcode & 0x40) > 0 ? 2 : 1), out xArg0);
                    LoadOperand((byte)((opcode & 0x20) > 0 ? 2 : 1), out xArg1);
                    
                    _op2Opcodes[opcode & 0x1f].Execute(xArg0, xArg1);
                }
                else if (opcode < 0xb0) // 1OP opcodes
                {
                    LoadOperand((byte)(opcode >> 4), out xArg0);
                    
                    _op1Opcodes[opcode & 0x0f].Execute(xArg0);
                }
                else if (opcode < 0xc0) // 0OP opcodes
                {
                    _op0Opcodes[opcode - 0xb0].Execute();
                }
                else // VAR opcodes
                {
                    _machine.Memory.CodeByte(out byte xSpecifier1);
                    int xArgsCount = LoadAllOperands(xSpecifier1, out xArg0, out xArg1, out var xArg2, out var xArg3);

                    // Call opcodes with up to 8 arguments
                    if (opcode == 0xec || opcode == 0xfa)
                    {
                        _machine.Memory.CodeByte(out byte xSpecifier2);
                        xArgsCount += LoadAllOperands(xSpecifier2, out xArg4, out xArg5, out xArg6, out xArg7);
                    }

                    _varOpcodes[opcode - 0xc0].Execute(xArg0, xArg1, xArg2, xArg3, xArg4, xArg5, xArg6, xArg7,
                        (ushort) xArgsCount, Interpret);
                    return;
                }
            } while (Finished == 0);
        }

        /// <summary>
        /// Load an operand, either a variable or a constant.
        /// </summary>
        /// <param name="aType"></param>
        /// <param name="aArg"></param>
        private void LoadOperand(byte aType, out ushort aArg)
        {
            if ((aType & 0x02) > 0) // Variable
            {
                _machine.Memory.CodeByte(out byte xVariable);

                if (xVariable == 0)
                {
                    aArg = _machine.Memory.Stack.Pop();
                }
                else if (xVariable < 16)
                {
                    aArg = _machine.Memory.Stack[_machine.Memory.Stack.Bp - xVariable];
                }
                else
                {
                    ushort xAddress = (ushort) (_machine.Header.GlobalsOffset + 2 * (xVariable - 16));
                    _machine.Memory.GetWord(xAddress, out aArg);
                }
            }
            else if ((aType & 1) > 0) // Small Constant
            {
                _machine.Memory.CodeByte(out byte xValue);
                aArg = xValue;
            }
            else // Large Constant
            {
                _machine.Memory.CodeWord(out aArg);
            }
        }

        /// <summary>
        /// Given the operand specifier byte, load all (up to four) operands for a VAR or EXT opcode.
        /// </summary>
        /// <param name="aSpecifier"></param>
        /// <param name="aArg0"></param>
        /// <param name="aArg1"></param>
        /// <param name="aArg2"></param>
        /// <param name="aArg3"></param>
        private int LoadAllOperands(byte aSpecifier, out ushort aArg0, out ushort aArg1, out ushort aArg2, out ushort aArg3)
        {
            int xArgsCount = 0;
            aArg0 = 0;
            aArg1 = 0;
            aArg2 = 0;
            aArg3 = 0;

            byte xType = (byte)((aSpecifier >> 6) & 0x03);
            if (xType == 3)
            {
                return xArgsCount;
            }

            LoadOperand(xType, out aArg0);
            xArgsCount++;

            xType = (byte)((aSpecifier >> 4) & 0x03);
            if (xType == 3)
            {
                return xArgsCount;
            }

            LoadOperand(xType, out aArg1);
            xArgsCount++;

            xType = (byte)((aSpecifier >> 2) & 0x03);
            if (xType == 3)
            {
                return xArgsCount;
            }

            LoadOperand(xType, out aArg2);
            xArgsCount++;

            xType = (byte)((aSpecifier >> 0) & 0x03);
            if (xType == 3)
            {
                return xArgsCount;
            }

            LoadOperand(xType, out aArg3);
            xArgsCount++;

            return xArgsCount;
        }

        // TODO
        //private void Extended()
        //{
        //    _machine.Memory.CodeByte(out byte xOpcode);
        //    _machine.Memory.CodeByte(out byte xSpecifier);

        //    LoadAllOperands(xSpecifier);

        //    if (xOpcode < 0x1e)
        //    {
        //        Invoke(EXTOpcodes[xOpcode], xOpcode);
        //    }
        //}
    }
}
