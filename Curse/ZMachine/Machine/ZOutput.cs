using Curse.Windows;

namespace Curse.ZMachine.Machine
{
    public class ZOutput : IZOutput
    {
        private readonly Zork _screen;

        public ZOutput(Zork aScreen)
        {
            _screen = aScreen;
        }

        public void PrintZscii(short aChar)
        {
            if (aChar == 0)
            {
                return;
            }

            char ch = ZText.CharFromZscii(aChar);
            _screen.WriteChar(ch);
        }

        public void PrintString(string aString)
        {
            _screen.WriteString(aString);
        }
    }
}
