﻿using Cosmos.System.Graphics;
using Cosmos.System.Graphics.Fonts;
using Curse.Abstract;
using Curse.ImGUI;

namespace Curse
{
    static class Backdrop
    {
        private static readonly string[] BackgroundText =
        {
            "Welcome to the Curse desktop environment/operating system",
            "Curse is developed by JFronny",
            "",
            "You can use the menu at the top left to start applications/configure the system",
            "The menu is split into categories (left) and entries (right)",
            "Simply hover over a category to select it and click on an app to start it"
        };
        private static readonly Pen Pen = ColorConfs.Component;
        private static readonly Pen Taskbar = ColorConfs.Component;
        public static void Draw(BufferCanvas canvas, Font font, WindowManager manager)
        {
            int bgTextStart = (canvas.ScreenY - BackgroundText.Length * font.Height) / 2;
            canvas.DrawFilledRectangle(Taskbar, 0, 0, canvas.ScreenX, font.Height + manager.Padding * 4);
            for (var i = 0; i < BackgroundText.Length; i++)
                canvas.DrawStringCentered(BackgroundText[i], font, Pen, bgTextStart + font.Height * i);
            canvas.DrawString("Curse (c) JFronny", font, Pen, canvas.ScreenX - 17 * font.Width, canvas.ScreenY - font.Height);
        }
    }
}
